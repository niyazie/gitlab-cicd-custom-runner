FROM docker:dind
RUN echo 'http://dl-cdn.alpinelinux.org/alpine/latest-stable/main/' >> /etc/apk/repositories && \
    apk --no-cache update
# Install pip, poetry and tools
RUN apk --no-cache add python3=3.11.2-r0 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/main 
RUN apk add py3-pip=23.0.1-r0 --repository=http://dl-cdn.alpinelinux.org/alpine/edge/community
RUN python -m pip install -U pip && python -m pip install poetry poetry-dynamic-versioning pylint
